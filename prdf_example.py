import numpy as np

from ase.build import bulk
import matplotlib.pyplot as plt

from prdf import prdf

rmax = 20.0
dr = 0.2
eta = 0.1
atoms = bulk('Si')
atoms.set_chemical_symbols(['Si', 'F'])
rs = np.arange(0, rmax - dr, dr / 5)
p_ttr, types = prdf(atoms, rs, dr, rmax)
print('ordering:')
for i, t in enumerate(types):
    print('{0}, {1}'.format(i, t))

ntypes = len(types)
for i in range(ntypes):
    for j in range(ntypes):
        plt.plot(rs, p_ttr[i, j], label='{0}, {1}'.format(types[i],
                                                          types[j]))

plt.legend()
plt.show()


